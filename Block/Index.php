<?php
namespace Gamma\ITP\Block;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
    public function getValue($value){
        return $this->_scopeConfig->getValue('gamma_general/general_configurations/'.$value);
    }
}